Compilacion de la libreria OpenCV 4 en windows 10 con CUDA 10 para VisualStudio 2017 15

Se siguieron las siguientes instrucciones, existen algunos errores pequenos e.g. espacios en blanco, llaves, etc. 

https://jamesbowley.co.uk/build-opencv-4-0-0-with-cuda-10-0-and-intel-mkl-tbb-in-windows/

```bash
copy "%openCvBuild%\install\python\cv2\python-3.7\cv2.cp37-win_amd64.pyd" "%USERPROFILE%\Anaconda3\env\compresion_video\site-packages\cv2.cp37-win_amd64.pyd" 
python
>>import cv2
```

##Compilacion de OpenCV4 con CUDA 10 en Centos 7 

```bash
yum install gcc gcc-c++ cmake3 git ffmpeg-devel blas-devel lapack-devel atlas-devel gtk3-devel

#Instalar en un entorno virtual

conda create --name compresion_video python=3.7.1

conda activate compresion_video

conda install scikit-video
conda install matplotlib
conda install statsmodels=0.9
conda install numpy

git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git

cd opencv
mkdir build
cd build

#La variable PYTHON3_EXECUTABLE evita que CMAKE utilice python2 para compilar. 

cmake3 -B /home/sistemas/instaladores/opencv/build/ -S /home/sistemas/instaladores/opencv/ \
-D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/home/sistemas/local -D INSTALL_C_EXAMPLES=ON \ 
-D BUILD_opencv_world=ON -D BUILD_opencv_gapi=OFF -D WITH_NNCUVID=OFF -D WITH_CUDA=ON -D CUDA_FAST_MATCH=ON \
-D WITH_CUBLAS=ON -D INSTALL_C_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D OPENCV_EXTRA_MODULES_PATH=/home/sistemas/instaladores/opencv_contrib/modules \
-D OPENCV_ENABLE_NONFREE=ON -D CUDA_ARCH_PTX=7.5 -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_opencv_python3=ON   \
-D PYTHON3_EXECUTABLE=/home/sistemas/anaconda3/envs/compresion_video/bin/python3.7 -D PYTHON3_LIBRARIES=/home/sistemas/anaconda3/envs/compresion_video/lib/libpython3.7m.so \
-D PYTHON_DEFAULT_EXECUTABLE=/home/sistemas/anaconda3/envs/compresion_video/bin/python3.7 ..

make
make install 

```

Agregar la $HOME/local al path del entorno virtual de conda 

```cmd

ln -s /home/sistemas/local/lib/python3.7/site-packages/cv2 anaconda3/envs/compresion_video/lib/python3.7/site-packages/cv2 
```

##Compilacion en ubuntu 16, conda environment
```cmd
apt-get install libswscale-dev  libavformat-dev libavcodec-dev libgtk2.0-dev

```
