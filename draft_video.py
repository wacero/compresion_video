"""
Created on Fri Mar 29 15:08:34 2019

@author: Stalin featuring wacero
"""
from matplotlib import pyplot as plt
import cv2
import skvideo.io
import pandas as pd

import scipy as sp
import datetime
import csv
import numpy as np
import os
import subprocess
import statsmodels.api as sm




video_process_figure=plt.figure()
axis_original=video_process_figure.add_subplot(1,5,1)
axis_original.set_title('frame_original')
axis_hsv=video_process_figure.add_subplot(1,5,2)
axis_hsv.set_title('frame_HSV')
axis_contour=video_process_figure.add_subplot(1,5,3)
axis_contour.set_title("frame_contour")
axis_binary=video_process_figure.add_subplot(1,5,4)
axis_binary.set_title("frame in black and white")
axis_segment=video_process_figure.add_subplot(1,5,5)
axis_segment.set_title("frame segmented")

'''
Videos de prueba:
/home/sistemas/videos_prueba/RUMINAHUI-20151223-0400_nublado.mp4
/home/sistemas/videos_prueba//RUMINAHUI-20151224-1740_nublado_dia.mp4
/home/sistemas/videos_prueba/RUMINAHUI-20151223-1200_despejado.mp4

OVT-20160511-2040  OVT-20160511-1210 COPTVS-20190101-2250.mp4 
RUMINAHUI-20151222-0450 RUMINAHUI-20151222-1220 RUMINAHUI-20151222-0340
OVT-20160511-2040.mp4
'''

t=0
tiempo = [0]
blancos = []
blancos=np.empty(1,dtype=float)
video_file="/home/sistemas/videos_prueba/RUMINAHUI-20151223-1200_despejado.mp4"

open_video = cv2.VideoCapture(video_file)
 
if (open_video.isOpened()== False):
    print("Error opening video_file stream or file")

video_metadata=skvideo.io.ffprobe(video_file)
video_duration  = float(video_metadata['video']['@duration'])

lower_blue = np.array([0,0,100])
upper_blue = np.array([0,0,255])

while(open_video.isOpened()):
    
    frame_status, frame_original = open_video.read()
    if frame_status == True:
        
        frame_hsv = cv2.cvtColor(frame_original, cv2.COLOR_RGB2HSV)

        frame_contour = cv2.inRange(frame_hsv, lower_blue, upper_blue)
        threshold_status,frame_binary = cv2.cuda.threshold(frame_contour,128,255,cv2.THRESH_BINARY)
        #img[y0:y1, x0:x1]
        frame_segment = frame_binary[150:450, 200:600] 
        
        #cv2.imshow('HSV',frame_hsv)
        #cv2.imshow('frame_contour',frame_contour)
        #cv2.imshow('B&B',frame_binary)
        #cv2.imshow('frame_segment', frame_segment)
        #cuenta la cantidad de blancos en la imagen binarizada
        n_white_pix = np.sum(frame_segment == 255)
        t += 1
        t1=(t/video_duration)
        
        blancos=np.append(blancos,n_white_pix)
        tiempo= np.append(tiempo,t1)    

        axis_original.imshow(frame_original)
        axis_hsv.imshow(frame_hsv)
        axis_contour.imshow(frame_contour)
        axis_binary.imshow(frame_binary)
        axis_segment.imshow(frame_segment)
        plt.pause(0.0001)
        
        print('Number of white pixels:', n_white_pix, t1)
        
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    
        # Break the loop
    else: 
        break

open_video.release()
cv2.destroyAllWindows()
cv2.waitKey(0)


#'''
nublado_pd=pd.DataFrame({'blancos':blancos,'tiempo':tiempo})
nublado_ciclo,nublado_tend=sm.tsa.filters.hpfilter(nublado_pd['blancos'])
nublado_pd['tend']=nublado_tend
nublado_pd[['blancos','tend']].plot()


fig,ax = plt.subplots(1)
ax.plot(tiempo,blancos)
lst= np.full((t+1), 60000)
idx = np.argwhere(np.diff(np.sign(lst - nublado_tend))).flatten()

plt.plot(tiempo[idx],nublado_pd['tend'][idx], 'ro')

nublado_pd['umbral']=lst
nublado_pd.plot()
plt.show()
ventanas=np.array(tiempo[idx])
inicio= 0
ventanas=np.insert(ventanas,0,0)
ventanas=np.insert(ventanas,len(ventanas),t1)
ventanas1 = np.array([0,0,0])
for i in range(0,len(ventanas)-1):
    if i%2==0 :
        process=0
    else:
        process=1       
    #ventanas1=(ventanas[i],ventanas[i+1],process)
    ventanas1 = np.vstack([ventanas1,[ventanas[i],ventanas[i+1],process]])
    print(ventanas1)
    
np.savetxt("C:\\videos\\output.txt", ventanas1,'%.1f', delimiter=",")


#'''
