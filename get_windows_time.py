# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 14:58:51 2019
    
@author: Stalin feature wacero@igepn.edu.ec

   
"""        
import cv2 
import time
import numpy as np
import skvideo.io
import skvideo.datasets
from matplotlib import pyplot as plt
import subprocess
from fractions import Fraction
import statsmodels.api as sm
import pandas as pd

#######################
#video="C:\\Users\\Stalin\\salida.mp4"
#video="C:\\videos\\salida10.mp4"
#video="C:\\videos\\out60.mp4"
#######################
video="/home/sistemas/video_20min.mp4"
videodata = skvideo.io.vreader(video)
t=0
tiempo = [0]
blancos = []
blancos=np.empty(1,dtype=float)

BLACK=128
WHITE=255
    
def getTime(input_video):

    video_metadata=skvideo.io.ffprobe(input_video)
    return float(video_metadata['video']['@duration'])

duration_video  = getTime(video) 

start =time.time() # initializes the count time of program execution
#print(duration_video)

for frame in videodata:
    gpu_storage = cv2.cuda_GpuMat()
    gpu_storage.upload(frame)
    hsv_image = cv2.cuda.cvtColor(gpu_storage, cv2.COLOR_RGB2HSV)
    hsv_image_array=hsv_image.download()
    #hsv_image = cv2.cvtColor(hsv_image_array, cv2.COLOR_RGB2HSV)
    #print(hsv_image)
    lower_cloud_rgb = np.array([0,0,100])
    upper_cloud_rgb = np.array([0,0,255])
    # Threshold the HSV image to get only blue colors
    check_filter_threshold = cv2.inRange(hsv_image_array, lower_cloud_rgb, upper_cloud_rgb)
    
    
    ret,image_contour = cv2.cuda.threshold(check_filter_threshold,BLACK,WHITE,cv2.THRESH_BINARY)
    
    roi = image_contour[150:450, 200:600]
    n_white_pix = np.sum(roi == WHITE)
    #print(n_white_pix)
    #cv2.imshow('roi',roi)
    t += 1
    t1=(t/duration_video)
    #print('Number of white pixels:', n_white_pix, t1)
    
    #np.savetxt("C:\\videos\\output10.txt", n_white_pix, '%.1f',t1)
    blancos=np.append(blancos,n_white_pix)
    tiempo= np.append(tiempo,t1)
####################################################### imprimir blancos vs tiempo (frames por segundo)
#print(n_white_pix, t1)   
#############################################################
    
nublado_pd=pd.DataFrame({'blancos':blancos,'tiempo':tiempo})

#nublado_pd.plot(nublado_pd['blancos'])
#plt.show()


nublado_ciclo,nublado_tend=sm.tsa.filters.hpfilter(nublado_pd['blancos'])
nublado_pd['tend']=nublado_tend
nublado_pd[['blancos','tend']].plot()

print(tiempo)

x1 = np.linspace(0,60,t+1)
y1 = 2000*x1 
#print(blancos)
fig,ax = plt.subplots(1)
ax.plot(tiempo,blancos)
lst= np.full((t+1), y1)
idx = np.argwhere(np.diff(np.sign(lst - nublado_tend))).flatten()
    
plt.plot(tiempo[idx],nublado_pd['tend'][idx], 'ro')

nublado_pd['umbral']=lst
nublado_pd.plot()
plt.show()
#print(nublado_tend)
ventanas=np.array(tiempo[idx])
inicio= 0
ventanas=np.insert(ventanas,0,0)
ventanas=np.insert(ventanas,len(ventanas),t1)
ventanas1 = np.array([0,0,0])
for i in range(0,len(ventanas)-1):
    if i%2==0 :
        process=0
    else:
        process=1       
    #ventanas1=(ventanas[i],ventanas[i+1],process)
    ventanas1 = np.vstack([ventanas1,[ventanas[i],ventanas[i+1],process]])
    #print(ventanas1)
################################    
#print(blancos/)
np.savetxt("C:\\videos\\output.txt", ventanas1,'%.1f', delimiter=",")
#np.savetxt("C:\\videos\\output10.txt",blancos,'%.1f')
end = time.time()
#print(end-start)    