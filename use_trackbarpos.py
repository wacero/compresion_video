
import cv2
import numpy as np
'''
Videos de prueba:
/home/sistemas/videos_prueba/RUMINAHUI-20151223-0400_nublado.mp4
/home/sistemas/videos_prueba//RUMINAHUI-20151224-1740_nublado_dia.mp4
/home/sistemas/videos_prueba/RUMINAHUI-20151223-1200_despejado.mp4

OVT-20160511-2040  OVT-20160511-1210 COPTVS-20190101-2250.mp4 
RUMINAHUI-20151222-0450 RUMINAHUI-20151222-1220 RUMINAHUI-20151222-0340
OVT-20160511-2040.mp4
'''

def nothing(x):
    pass

cv2.namedWindow('video_volcan')

cv2.createTrackbar("upper_blue",'video_volcan',50,255,nothing)
cv2.createTrackbar("lower_blue",'video_volcan',50,255,nothing)


video_file="/home/sistemas/videos_prueba//RUMINAHUI-20151224-1740_nublado_dia.mp4"

open_video = cv2.VideoCapture(video_file)


while(open_video.isOpened()):
    
    
    
    frame_status, frame_original = open_video.read()
    if frame_status == True:
        
        upper_blue_value=cv2.getTrackbarPos('upper_blue','video_volcan')
        lower_blue_value=cv2.getTrackbarPos('lower_blue','video_volcan')
        
        upper_blue=np.array([0,0,upper_blue_value])
        lower_blue=np.array([0,0,lower_blue_value])
        
        frame_hsv = cv2.cvtColor(frame_original, cv2.COLOR_RGB2HSV)
        frame_contour = cv2.inRange(frame_hsv, lower_blue, upper_blue)
        
        #print(frame_contour)
        cv2.imshow("video_volcan",frame_contour)
    
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    else:
        break

open_video.release()
cv2.destroyAllWindows()
cv2.waitKey(0)
